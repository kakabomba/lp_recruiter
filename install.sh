#!/bin/bash

python3 -m venv .venv
source ./.venv/bin/activate
pip3 install -r ./requirements.txt

mkdir data
pv application_data.csv.gz | gzip -d > ./data/application_data.csv
