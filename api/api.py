from flask import Flask, jsonify, request
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import control as c
import pickle, re

ALL_CAND_FILE_NAME = './data/all_candidates.pkl'

def in_range(f, t, v = None, type_ = int):
    if f>t:
        f, t = t, f

    return type_((f+t)/2) if v is None else min([max([f, type_(v)]), t])

def int_from_req(name, default, f = None, t = None):
    ret = request.form.get(name, default)
    if f is None or t is None:
        return ret
    else:
        return int(in_range(f, t, ret))


def get_models_from_request():
    META_BEST_COUNT = int_from_req('META_BEST_COUNT', 2, 0, 5)
    META_WORST_COUNT = int_from_req('META_WORST_COUNT', 1, 0, 5 - META_BEST_COUNT)
    WORD_BEST_COUNT = int_from_req('WORD_WORST_COUNT', 10, 0, 200)
    WORD_WORST_COUNT = int_from_req('WORD_WORST_COUNT', 2, 0, 200)
    return META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT


app = Flask(__name__)

@app.route('/lp/api/')
def index():
    return "LP API"

@app.route('/lp/api/v0.0/score/', methods=['POST'])
def score_application():
    return jsonify({'TODO': ''})

@app.route('/lp/api/v0.0/application/', methods=['PUT'])
def add_application():
    return jsonify({'TODO': ''})

@app.route('/lp/api/v0.0/features/', methods=['POST'])
def extract_features():

    META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT = get_models_from_request()

    full_set, training_set, validation_set = c.get_set()
    with open(ALL_CAND_FILE_NAME, 'wb') as output:
        pickle.dump(full_set, output, pickle.HIGHEST_PROTOCOL)

    applications_training_set = c.c2a(training_set)
    applications_validation_set = c.c2a(validation_set)

    vectors_representative = c.find_features(applications_training_set, META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT)
    with open(c.file_name_for_model(META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT), 'wb') as output:
        pickle.dump(vectors_representative, output, pickle.HIGHEST_PROTOCOL)

    model = c.train(applications_training_set, vectors_representative)

    validation_feature_vectors = [[sample.get_feature(v.name) for v in vectors_representative]
                                  for sample in applications_validation_set]

    predictions = model.predict(validation_feature_vectors)

    predictions_by_candidate = c.group_by_candidates(applications_validation_set, predictions)
    model_efficiency = c.calculate_model_efficiency(predictions_by_candidate)

    # eprint(model_efficiency)
    return jsonify({'training_set_length': len(training_set),
                    'vectors':
                        [{v.name: {'correlation': v.correlation, 'p_value': v.p_value} for v in vectors_representative}],
                    'validation_set_length': len(validation_set),
                    'validation_results': predictions_by_candidate,
                    'model_efficiency': model_efficiency,
                    })

@app.route('/lp/api/v0.0/predict/', methods=['POST'])
def predict():

    try:
        META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT = get_models_from_request()

        if path.isfile(ALL_CAND_FILE_NAME):
            with open(ALL_CAND_FILE_NAME, 'rb') as input:
                all_candidates = pickle.load(input)
        else:
            with open('./data/application_data.csv', newline='\n') as csvfile:
                all_candidates = c.read_candidates(csvfile)

        with open(c.file_name_for_model(META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT), 'rb') as input:
            vectors_representative = pickle.load(input)

        model = c.train(c.c2a(all_candidates), vectors_representative)
        lines = request.data.decode("utf-8")
        lines = lines.splitlines()
        candidates = c.read_candidates((l for l in lines))
        if len(candidates) < 1:
            raise Exception("Please provide at least one valid line")
        applications = c.c2a(candidates)

        feature_vectors = [[sample.get_feature(v.name) for v in vectors_representative] for sample in applications]

        predictions = model.predict(feature_vectors)

        predictions_by_candidate = c.group_by_candidates(applications, predictions)

        return jsonify({'predictions': predictions_by_candidate, 'error': ''})

    except Exception as e:
        return jsonify({'predictions': [], 'error': e.__str__()})


@app.route('/lp/api/shutdown/', methods=['GET'])
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

if __name__ == '__main__':
    app.run(debug=True)

