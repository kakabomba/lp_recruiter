import time, datetime
import json, re
import functools, itertools, collections
import scipy.stats

def parse_date(s):
    return time.mktime(datetime.datetime.strptime(re.sub(r':(..)$', r'\1', s), '%Y-%m-%d %H:%M:%S%z').timetuple())

class Vector:
    values = None
    name = None
    score_vector = None
    correlation = None
    p_value = None


    def __str__(self):
        return "Vector <{}>: name=`{}` corr={:10.8f} p={:10.8f}".format(id(self), self.name, self.correlation, self.p_value)

    def __init__(self, values, name):
        self.name = name
        self.values = values
        self.correlation, self.p_value = scipy.stats.pearsonr(Vector.score_vector, self.values)


class Application:
    id = None
    length = None
    score = None
    words = None
    speech_confidence = None
    candidate = None
    words_count_different = None
    words_count_total = None
    invitationTime = None
    applicationTime = None
    anticipation = None

    def __str__(self):
        return "Application score={:4.2f} #words={:d} candidate={}"\
            .format(self.score, len(self.words.items()), self.candidate.id if self.candidate else None)

    def get_feature(self, name):
        if name[0:2] == '__':
            return self.get_meta_feature(name)
        else:
            return self.words[name][0] if name in self.words else 0

    def get_meta_feature(self, name):
        # TODO: early birds is WRONG feature. Almost certainly it is human bias. Or not?
        if name == '__video length':
            return self.length / 1.
        elif name == '__speech confidence':
            return self.speech_confidence
        elif name == '__anticipation':
            return self.anticipation
        elif name == '__speech diversity':
            return 1 - self.words_count_different / (self.words_count_total + 1e-9)
        elif name == '__early birds':
            return -self.applicationTime
        else:
            raise Exception('Wrong meta feature named {}'.format(name))

    @staticmethod
    def get_meta_keys():
        return ['__video length', '__speech confidence', '__speech diversity', '__early birds', '__anticipation']

    # def get_meta_features(self):
    #     return [(self.get_meta_feature(name), name) for name in self.get_meta_keys()]


    @staticmethod
    def extract_attrs(data):
        return {'id': data[0],
                'invitationTime': parse_date(data[2]), 'isRetake': 1 if data[3] == 'True' else 0,
                'applicationTime': parse_date(data[6]),
                'length': float(data[7]), 'score': float(data[8])}

    def parse_word(w):
        pass

    def __init__(self, data, candidate):
        self.candidate = candidate
        for k,v in self.extract_attrs(data).items():
            self.__setattr__(k,v)
        self.words = {}
        self.speech_confidence = 0
        all_words = json.loads(data[4])
        for w in all_words:
            name = w['name'].lower().strip()
            confidence = float(w['confidence'])
            self.speech_confidence += confidence
            if name not in self.words:
                self.words[name] = [0, 0.]
            self.words[name][0] += 1
            self.words[name][1] += confidence

        self.words_count_total = len(all_words)
        self.speech_confidence /= self.words_count_total + 1e-9
        self.words_count_different = len(self.words)
        self.anticipation = self.applicationTime - self.invitationTime

        # assert abs(sum([v[0] for k, v in self.words.items()]) - 1.) < 1e-8


class Candidate:
    id = None
    isRetake = None
    applications = None

    length = None
    score = None
    words = None
    speech_confidence = None
    words_count_total = None
    words_count_different = None
    anticipation = None



    @staticmethod
    def extract_attrs(data):
        return {'id': data[1]}

    def __init__(self, data):
        self.words = {}
        self.applications = []
        for k,v in self.extract_attrs(data).items():
            self.__setattr__(k,v)

    def __str__(self):
        return "Candidate score={:4.2f} words={:d} #applications={:d}".format(self.score, len(self.words.items()),
                                                                     len(self.applications))

    def append_application(self, data, application):
        for k,v in self.extract_attrs(data).items():
            if self.__getattribute__(k) != v:
                print("candidate id=`{}` warning: `{}`=`{}` != {}".format(self.id, k, self.__getattribute__(k), v))
        self.applications.append(application)

    def join_applications(self):
        self.words = {}
        for a in self.applications:
            for name, cnt_conf in a.words.items():
                if name not in self.words:
                    self.words[name] = [0, 0.]
                self.words[name][0] += cnt_conf[0]
                self.words[name][1] += cnt_conf[1]

        self.words_count_different = len(self.words.items())
        self.words_count_total = sum([cnt_conf[0] for name, cnt_conf in self.words.items()])
        self.speech_confidence = sum([cnt_conf[1]/cnt_conf[0] for name, cnt_conf in self.words.items()]) / (self.words_count_different + 1e-9)
        self.words = {name: (cnt_conf[0], cnt_conf[1]/cnt_conf[0]) for name, cnt_conf in self.words.items()}
        self.score = sum([a.score for a in self.applications])/(len(self.applications))
        self.length = sum([a.length for a in self.applications])

        return self




