import statistics, math
import functools, itertools, collections, random
import models as m
import __del_mprint as pr
import numpy as np
import pickle

META_FEATURES_PRO_COUNT = 0
META_FEATURES_CON_COUNT = 0

WORDS_PRO_COUNT = 3
WORDS_CON_COUNT = 0

from sklearn import linear_model, datasets

def score2decision(score):
    return score


#
#
# full_set, training_set, validation_set = m.read_candidates(), [], []
# pr.print_candidates(full_set)
#
# full_set, applications_training_set, applications_validation_set = list(itertools.chain(*[c.applications for c in full_set])), [], []
# sampleClass = m.Application
#
# for ind, sample in enumerate(full_set):
#     (training_set if random.random() > 0.1 else validation_set).append(sample)
#
# m.Vector.score_vector = [score2decision(sample.score) for sample in training_set]
#
# meta_vectors_correlations = [m.Vector([sample.get_meta_feature(meta_name) for sample in training_set], meta_name)
#                              for meta_name in sampleClass.get_meta_keys()]
#
#     # for name in ['__video length', '__speech confidence','__speech diversity' ,'__anticipation','__early birds']]
# # meta_vectors.sort(key=lambda v: abs((1.0-v.p_value) * v.correlation), reverse=True)
#
# # meta_vectors = m.Vector.sort()
# # for name in ['__video length', '__speech confidence','__speech diversity']])
#
# pr.vectots(meta_vectors_correlations, 'all meta vectors')
# meta_vectors_correlations.sort(key=lambda v: v.correlation*(1-v.p_value), reverse=True)
# vectors_representative = meta_vectors_correlations[0:2] + meta_vectors_correlations[-1:0]
# pr.vectots(vectors_representative, 'most representative meta vectors')
#
# # training_features_vectors = [[sample.get_meta_feature(v.name) for v in meta_vectors_representative] for sample in training_set]
#
#
#
# all_words = list(itertools.chain(*[c.words.keys() for c in training_set]))
# all_words = dict(collections.Counter(all_words))
#
# # word_vectors_correlations = [m.Vector([sample.get_feature(w) for sample in training_set], w) for w in all_words]
# # with open('./data/word_vectors_correlations.pkl', 'wb') as output:
# #     pickle.dump(word_vectors_correlations, output, pickle.HIGHEST_PROTOCOL)
# with open('./data/word_vectors_correlations.pkl', 'rb') as input:
#     word_vectors_correlations= pickle.load(input)
#
# word_vectors_correlations.sort(key=lambda v: v.correlation, reverse=True)
#
# vectors_representative = vectors_representative + word_vectors_correlations[0:50] + word_vectors_correlations[-10:]
# pr.vectots(vectors_representative, 'most representative words')
#
# training_features_vectors = [[sample.get_feature(v.name) for v in vectors_representative] for sample in training_set]
# training_outcome_values = [score2decision(c.score) for c in training_set]
#
# regression = linear_model.LinearRegression()
# regression.fit(training_features_vectors, training_outcome_values)
#
#
# # regression.fit(features_vectors, outcome_values)
# # validation = [([m.Vector.get_meta_feature(c, v.name) for v in representative_meta_vectors], score2decision(c.score)) for c in candidates_validation_set]
# # prediction = regression.predict([v[0] for v in validation])
# #
#
#
# validation_feature_vectors = [[sample.get_feature(v.name) for v in vectors_representative] for sample in validation_set]
# prediction = regression.predict(validation_feature_vectors)
#
# guess_score_comparision = [
#         {'index': ind,
#          'score': score2decision(sample.score), 'score_predicted': prediction[ind],
#          'recruited': 0, 'recruited_predicted': 0} for ind, sample in enumerate(validation_set)]
#
# recruit_threshold = 0.1
# guess_score_comparision.sort(key=lambda x: x['score'], reverse=True)
# for ind in range(0, round(len(guess_score_comparision) * recruit_threshold)):
#     guess_score_comparision[ind]['recruited'] = 1
#
# guess_score_comparision.sort(key=lambda x: x['score_predicted'], reverse=True)
# positive = sum(map(lambda g: g['recruited'], guess_score_comparision))
#
# print("Testing set length={:d},   positive set size={:d}\n"
#       .format(len(guess_score_comparision), int(len(guess_score_comparision)*recruit_threshold)))
#
# for multiplier in [1, 2, 3]:
#
#     for ind in range(0, len(guess_score_comparision)):
#         guess_score_comparision[ind]['recruited_predicted'] = 1 if ind <= round(
#             len(guess_score_comparision) * recruit_threshold * multiplier) else 0
#     false_negative = sum(map(lambda g: g['recruited_predicted'] == 0 and g['recruited'] == 1, guess_score_comparision)) / positive
#
#     predicted_positive = sum(map(lambda g: g['recruited_predicted'], guess_score_comparision))
#     random_positive = len(guess_score_comparision) * recruit_threshold * multiplier
#     improvement = predicted_positive/random_positive
#
#     print(
#         "hired_threshold={:4.2f}   select_threshold={:4.2f}   false_negative={:5.3f}"
#             .format(recruit_threshold,recruit_threshold*multiplier,false_negative, improvement, predicted_positive, int(random_positive)))
#
# for ind in range(0, len(guess_score_comparision)):
#     guess_score_comparision[ind]['recruited_predicted'] = 1 if ind <= round(len(guess_score_comparision) * recruit_threshold) else 0
#
# my_guess_print = ["{recruited} {recruited_predicted}           | {score:10.8f} {score_predicted:10.8f}".format(**mg)
#                   for mg in guess_score_comparision]

# print(guess_score_comparision)

# pr.print_predictions(validation_feature_vectors, prediction)

