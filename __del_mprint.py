import itertools

def print_candidates(candidates):
    print("`total candidates: {}`".format(len(candidates)))
    l = lambda c: len(c.applications)
    for k, g in itertools.groupby(sorted(candidates, key = l), l):
        print("`{}` candidates with `{}` application(s)".format(len(list(g)), k))




def vectots(vectors, text):
    print("\n --- {} --- \n".format(text))
    for v in vectors:
        print(v)
    print("\n")


def print_predictions(validation_feature_vectors, prediction):
    my_guess = [
        {'index': ind, 'score': v[1], 'score_predicted': prediction[ind], 'recruited': 0, 'recruited_predicted': 0} for
        ind, v in enumerate(validation_feature_vectors)]
    recruit_threshold = 0.1
    my_guess.sort(key=lambda x: x['score'], reverse=True)
    for ind in range(0, round(len(my_guess) * recruit_threshold)):
        my_guess[ind]['recruited'] = 1

    my_guess.sort(key=lambda x: x['score_predicted'], reverse=True)
    positive = sum(map(lambda g: g['recruited'], my_guess))

    for multiplier in [1, 2, 3]:

        for ind in range(0, len(my_guess)):
            my_guess[ind]['recruited_predicted'] = 1 if ind <= round(
                len(my_guess) * recruit_threshold * multiplier) else 0
        false_negative = sum(map(lambda g: g['recruited_predicted'] == 0 and g['recruited'] == 1, my_guess)) / positive
        print(
            "recruit_threshold={:4.2f}   positive_threshold={:4.2f}   false_negative={:5.3f}".format(recruit_threshold,
                                                                                                     multiplier,
                                                                                                     false_negative))

    for ind in range(0, len(my_guess)):
        my_guess[ind]['recruited_predicted'] = 1 if ind <= round(len(my_guess) * recruit_threshold) else 0

    my_guess_print = ["{recruited} {recruited_predicted}           | {score:10.8f} {score_predicted:10.8f}".format(**mg)
                      for mg in my_guess]

    print(my_guess)