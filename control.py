import itertools, collections
import models as m
import csv, random, math
import functools
from sklearn import linear_model, datasets
from os import sys, path
import pickle

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def read_candidates(csvfile):
    # TODO: move application time min/max to persistent model
    i = -1
    candidates = {}


    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        # if i > 0:
        try:
            i +=1
            candidate = candidates[row[1]] if row[1] in candidates else m.Candidate(row)
            application = m.Application(row, candidate)
            if len(application.words):
                candidate.append_application(row, application)
                candidates[candidate.id] = candidate
            else:
                raise Exception('Zero words')

        except Exception as e:
            eprint("Line with index=`{}` ignored with error `{}`\n{}".format(i, e, row))

    earliest, latest = None, None
    for c_id ,c in candidates.items():
        for a in c.applications:
            earliest = a.applicationTime if earliest is None else min([earliest, a.applicationTime])
            latest = a.applicationTime if latest is None else max([latest, a.applicationTime])
    # print(earliest, latest)

    for c_id, c in candidates.items():
        for a in c.applications:
            a.applicationTime = earliest if latest == earliest else (a.applicationTime - earliest) / (latest - earliest)

    candidates = [c.join_applications() for _, c in candidates.items()]

    return candidates

def file_name_for_model(META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT):
    return './data/model_{}_{}_{}_{}.pkl'.format(META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT)

def get_set():

    with open('./data/application_data.csv', newline='\n') as csvfile:
        full_set, training_set, validation_set = read_candidates(csvfile), [], []

    for ind, sample in enumerate(full_set):
        (training_set if random.random() > 0.1 else validation_set).append(sample)

    return full_set, training_set, validation_set



def find_features(training_set, META_BEST_COUNT, META_WORST_COUNT, WORD_BEST_COUNT, WORD_WORST_COUNT):
    # TODO MULTITHREADING HERE

    sampleClass = m.Application

    m.Vector.score_vector = [sample.score for sample in training_set]

    meta_vectors_correlations = [m.Vector([sample.get_meta_feature(meta_name) for sample in training_set], meta_name)
                                 for meta_name in sampleClass.get_meta_keys()]
    meta_vectors_correlations.sort(key=lambda v: v.correlation * (1 - v.p_value), reverse=True)

    vectors_representative = []

    if META_BEST_COUNT or META_WORST_COUNT:
        if META_BEST_COUNT:
            vectors_representative += meta_vectors_correlations[0:META_BEST_COUNT]
        if META_WORST_COUNT:
            vectors_representative += meta_vectors_correlations[-META_WORST_COUNT:]

    if WORD_BEST_COUNT or WORD_WORST_COUNT:
        all_words = list(itertools.chain(*[c.words.keys() for c in training_set]))
        all_words = dict(collections.Counter(all_words))
        # TODO: remove duplicates, combine different forms with NLTK

        word_vectors_correlations = []
        ind = 0
        pp = -1
        for w, _ in all_words.items():
            if int(ind/len(all_words)*100) % 10 == 0:
                if pp != int(ind/len(all_words)*100):
                    pp = int(ind/len(all_words)*100)
                    print("%{:d}".format(pp))
            ind += 1
            word_vectors_correlations.append(m.Vector([sample.get_feature(w) for sample in training_set], w))

        word_vectors_correlations.sort(key=lambda v: v.correlation * (1 - v.p_value), reverse=True)

        if WORD_BEST_COUNT:
            vectors_representative += word_vectors_correlations[0:WORD_BEST_COUNT]

        if WORD_WORST_COUNT:
            vectors_representative += word_vectors_correlations[-WORD_WORST_COUNT:]


    return vectors_representative

def train(training_set, feature_vectors):
    # TODO pickle this also
    training_features_vectors = [[sample.get_feature(v.name) for v in feature_vectors]
                                 for sample in training_set]
    training_outcome_values = [c.score for c in training_set]

    regression = linear_model.LinearRegression()
    regression.fit(training_features_vectors, training_outcome_values)

    return regression

def c2a(candidates_set):
    return list(itertools.chain(*[c.applications for c in candidates_set]))

def group_by_candidates(applications_validation_set, predictions):

    validate_predictions = [{'score': sample.score, 'score_predicted': predictions[ind]}
                               for ind, sample in enumerate(applications_validation_set)]

    validate_predictions_for_candidates = {}
    for vi, vp in enumerate(validate_predictions):
        candidate_id = applications_validation_set[vi].candidate.id
        if candidate_id not in validate_predictions_for_candidates:
            validate_predictions_for_candidates[candidate_id] = []
        validate_predictions_for_candidates[candidate_id].append(vp)

    validate_predictions_for_candidates = [{
        'candidate_id': candidate_id,
        'score': functools.reduce(lambda s, a: s + a['score'], appls, 0) / len(appls),
        'score_predicted': functools.reduce(lambda s, a: s + a['score_predicted'], appls, 0) / len(appls)}
        for candidate_id, appls in validate_predictions_for_candidates.items()]

    return validate_predictions_for_candidates


def calculate_model_efficiency(validate_predictions_for_candidates):

    HIRING_THRESHOLD = 0.1
    validate_predictions_for_candidates.sort(key=lambda x: x['score'], reverse=True)

    for i, p in enumerate(validate_predictions_for_candidates):
        p['hired'] = 1 if i / len(validate_predictions_for_candidates) < HIRING_THRESHOLD else 0
    hired = sum(map(lambda g: g['hired'], validate_predictions_for_candidates))

    validate_predictions_for_candidates.sort(key=lambda x: x['score_predicted'], reverse=True)
    model_efficiency = []
    for multiplier in [1, 2, 3]:
        for i, p in enumerate(validate_predictions_for_candidates):
            p['selected'] = 1 if i / len(validate_predictions_for_candidates) < HIRING_THRESHOLD * multiplier else 0

        # predicted_positive = sum(map(lambda g: g['recruited_predicted'], guess_score_comparision))
        # random_positive = len(guess_score_comparision) * HIRING_THRESHOLD * multiplier
        # improvement = predicted_positive / random_positive

        model_efficiency.append({
            'hired_by_score': hired,
            'hired_in_selected': sum(map(lambda g: 1 if (g['selected'] == 1 and g['hired'] == 1) else 0,
                        validate_predictions_for_candidates)),
            'selected': int((math.ceil(len(validate_predictions_for_candidates) * HIRING_THRESHOLD * multiplier))),
            'false_negative':
                sum(map(lambda g: 1 if (g['selected'] == 0 and g['hired'] == 1) else 0,
                        validate_predictions_for_candidates)) / hired,
            'false_positive':
                sum(map(lambda g: 1 if (g['selected'] == 1 and g['hired'] == 0) else 0,
                        validate_predictions_for_candidates)) / hired
        })

    return model_efficiency

