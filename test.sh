#!/bin/bash

source ./.venv/bin/activate
python api/api.py &
#SERVER_PID=$!

while [[ "$RESULT" != "0" ]]; do
  curl -X POST http://localhost:5000/lp/api/v0.0/features/ 
  RESULT=$?
  sleep 1
done

cat ./data/application_data.csv| head -n100 | tail -n3 | curl -X POST  --data-binary  @- http://localhost:5000/lp/api/v0.0/predict/  --header "Content-Type:text/xml" 

curl http://localhost:5000/lp/api/shutdown/

#echo "killing $SERVER_PID"
#kill -9 $SERVER_PID
